RIEBEL THEME FOR DRUPAL 6.x

Introducing
-----------
The Riebel theme is under heavy development and should only be used for testing!


Background images
-----------------
You can set background images in the theme settings. If you want to add your own images you have to edit template.php and theme-settings.php or just rename one of the files with your own bitmap.
Make your own images!: The background images were created online here: http://www.stripegenerator.com/index.php?page=index 


Search
------
To enable the search menu block, set the checkbox in the theme settings and the checkbox in the user access role page.








Note: Forum node and comment template isn't ready yet. You can uncomment the CSS file in page.tpl.php and unset the _ in the filenames of
		node-forum.tpl.php and comment-forum.tpl.php to activate it.
		The forum files were copyied from the wonderful sky theme and converted to this theme.