<?php
// $Id: 

/**
 * Override theme_regions().
 */
function riebel_regions() {
  return array(
    'sidebar_left' 			=> t('Sidebar left'),
	'sidebar_left_second'	=> t('Sidebar left second'),
    'sidebar_right' 		=> t('Sidebar right'),	
    'content'       		=> t('Content'),
    'footer_left'   		=> t('Footer left'),
    'footer_middle' 		=> t('Footer middle'),
    'footer_right'  		=> t('Footer right'),
	'im_header'  			=> t('Im Header'),
  );
}

/*
* Initialize theme settings
*/
if (is_null(theme_get_setting('riebel_slidetabs'))) {  // <-- change this line
  global $theme_key;

  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the theme-settings.php file.
   */
  $defaults = array(             // <-- change this array
    'riebel_slidetabs' => 1,
    'riebel_taskbar' => 0,
	'riebel_logo' => 0,
	'riebel_registerplease' => 0,
	'riebel_breadcrumb' => 0,
	'riebel_bg_image' => 'bg_bars_blue.png',
	'riebel_float' => 'none',
	'riebel_taskbar_links' => '',
	'riebel_taskbar_ownlinks' => 0,
	'riebel_colormodule' => 1,
  );

  // Get default theme settings.
  $settings = theme_get_settings($theme_key);
  // Don't save the toggle_node_info_ variables.
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_' . $type]);
    }
  }
  // Save default theme settings.
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals.
  theme_get_setting('', TRUE);
}

function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 * Remove this to deactivate color module and use standard header image (dark) instead
 */
function phptemplate_preprocess_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
		//doesnt work because this is preprocess and the settings arent saved to the array...
		//if (theme_get_settings('riebel_colormodule') == "0") {
		_color_page_alter($vars);
	//}
  }
}


