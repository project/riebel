<?php

$info = array(

  // Pre-defined color schemes
  'schemes' => array(
	// base, link, gradation(top), gradation(bottom), text
	'#000000,#b6b6a0,#5c5c5c,#212121,#5d5d5b' => t('Riebel (Default)'),
	'#000000,#b6b6a0,#8af0df,#448882,#5d5d5b' => t('Aquamarine'),
	'#000000,#b6b6a0,#f46262,#521924,#5d5d5b' => t('Wine Red'),
	'#000000,#b6b6a0,#4764f0,#1c217d,#5d5d5b' => t('Deep Blue'),
	'#000000,#b6b6a0,#ef71ec,#7d1c7d,#5d5d5b' => t('Deep Purple'),
	'#000000,#b6b6a0,#efa9ee,#e265d0,#5d5d5b' => t('Barbie'),
	'#000000,#b6b6a0,#a3e779,#2c9b2e,#5d5d5b' => t('Green'),
	'#000000,#b6b6a0,#a8a8a8,#3d3d3d,#5d5d5b' => t('Grey'),

  ),

  // Images to copy over
  'copy' => array(
    'img/riebel_top_dark.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  // NOTE: this is new to Drupal 6.x
  'css' => array(
    'riebel_header.css',
  ),

  // Coordinates of gradient (x, y, width, height)
  'gradient' => array(0, 0, 800, 100),

  // Color areas to fill (x, y, width, height)
  'fill' => array(
    'base' => array(0, 100, 800, 500),
    'link' => array(0, 600,   0,   0),	// dummy 
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
	'img/riebel_top_dark.png'		=> array(  0,   0,   13, 100),


    'screenshot.png'            => array(  0,  20, 760, 400),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation
  'base_image' => 'color/base.png',
);
