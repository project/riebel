<?php // $Id:   ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
	
	<style type="text/css" media="all">
	body {
	<?php if (theme_get_setting('riebel_bg_image')): ?>
		background: url('<?php print base_path() . path_to_theme() ?>/img/<?php print theme_get_setting('riebel_bg_image') ?>') fixed;
	<?php endif; ?>

	float: <?php print theme_get_setting('riebel_float') ?>;
	}
	</style>
</head>

<body>

  <!-- Header Start -->
  <div id="header">
	<div id="headerInner">
		
		<?php /* Use this line to add graphical logo */
		if (theme_get_setting('riebel_logo')) { ?>
				<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><div id="prlogo"></div></a>
		<?php }
				else { ?>
		<?php //if ($site_name) { ?>
			<div id="siteName">
				<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a>
          </div>
		<?php } ?>
        <?php if ($site_slogan) { ?>
			<div id="siteSlogan">
				<?php print $site_slogan ?>
			</div>
		<?php } ?>
    	<div id="navigation"><?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?></div>
	</div>
	
	<div id="headerInnerBlock">
		<?php print $im_header ?>
    <?php print $search_box ?>        
	</div>
	
	<div id="headerInnerTabs">
		<?php if (theme_get_setting('riebel_slidetabs')): ?>
			<?php include('riebel_slidetabs.tpl.php'); ?>
		<?php endif; ?>
	</div>
	
  </div>
  	<div id="headershadow">
	</div>
  <!-- Header End -->
  
  <!-- Main Start -->
  <div id="maincontainer">

   <!-- Contentwrapper Start -->
   <div id="contentwrapper">
    <!-- Main Content Start -->
	<div id="contentcolumn">

		<!-- Display Main Content Start -->
		<div id="columns" style="width: 100%;">
          <?php if ($breadcrumb) { ?><div class="breadcrumb"><?php print $breadcrumb ?></div><?php } ?>
		  <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>		  
          <?php if ($title) { ?><h1 class="pageTitle"><?php print $title ?></h1><?php } ?>
          <?php if ($tabs) { ?><div class="tabs"><?php print $tabs ?></div><?php } ?>
          <?php if ($help) { ?><div class="help"><?php print $help ?></div><?php } ?>
          <?php if ($messages) { ?><div class="messages"><?php print $messages ?></div><?php } ?>


			<?php global $user;
				if ($user->uid) {		
					// add user loged in stuff here
					}
				else {
					// guest user should register
					 if (theme_get_setting('riebel_registerplease')): 
						include('riebel_registerplease.tpl.php'); 
					 endif;
				}
			?> 	


		  <?php print $content;?>
		  <?php print $feed_icons; ?>

		<!-- Display Main Content End -->
 	    </div>
	<!-- Contentwrapper End -->
	</div>
   <!-- Main Content End -->
   </div>



   
	<!-- sidebar left -->
	<div id="leftcolumn">
	          <?php if ($sidebar_left) { ?>
		      <div id="sidebarLeft">
			      <span id="topspan"></span>
					<div class="sidebarLeft-content">
				      <?php print $sidebar_left ?>
					</div>
			      <span id="bottomspan"></span>
		      </div>
		    <?php } ?>
		<?php if ($sidebar_left_second) { ?>
          <div id="sidebarLeftZwei">
            <?php print $sidebar_left_second ?>
          </div>
        <?php } ?>

	</div>

    <!-- sidebar right-->
	<div id="rightcolumn">
        <?php if ($sidebar_right) { ?>
          <div id="sidebarRight">
            <?php print $sidebar_right ?>
          </div>
        <?php } ?>

	</div>


	<!-- footer Start -->
	<div class="FooterContain">
		<div class="Footer">
			<div class="FooterLeft"><?php print $footer_left; ?></div>
			<div class="FooterMiddle"><?php print $footer_middle; ?></div>
			<div class="FooterRight"><?php print $footer_right; ?></div>      
		</div>
	<!-- footer End -->	 
    </div>
	
			<!-- Copyright Start -->
			<div class="copyright">
				<p>					<a href="<?php print $GLOBALS['base_url'] ?>/?q=rss.xml" title="Syndication"><acronym title="Really Simple Syndication">RSS</acronym></a>, 
					<a href="http://validator.w3.org/check?uri=referer" title="Valid XHTML"><acronym title="eXtensible Hyper Text Markup Language">XHTML</acronym></a>, 
					<a href="http://jigsaw.w3.org/css-validator/check/referer" title="Valid CSS"><acronym title="Cascading Style Sheet">CSS</acronym></a> | Riebel Theme by Mathias Dittrich <?php if ($footer_message) { ?> | <?php print $footer_message ?><?php } ?>
				</p>
			<!-- Copyright End -->
			</div>	
			
			
			<!-- Task Bar -->
		<?php if (theme_get_setting('riebel_taskbar')): ?>
				<?php include('riebel_taskbar.tpl.php'); ?>
		<?php endif; ?>
			
			
<!-- Contentweapper End -->
</div>
<?php print $closure ?>
</body>
</html>
