<?
// $Id

/*this bar was inspired by the very good Inanis Glass theme at http://www.inanis.net/blog
*/
?>
	<br style="background:#000;"/>
    <div class="taskbar">
        <!-- orb graphic  and check if user has admin rights -->
		 <?php if ($user->uid == 1) {?>
		<div class="nvtl"><ul><li><a href="?q=admin" alt="Admin"><span>- O -</span></a></li></ul></div>		
		<?php } ?>
		
		
				<?php if (theme_get_setting('riebel_taskbar_links') == 'secondary_links'): ?>
					<div id="taskbarlinks"><?php print theme('links', $secondary_links) ?></div>
				<?php elseif (theme_get_setting('riebel_taskbar_links') == 'primary_links'):  ?>
					<div id="taskbarlinks"><?php print theme('links', $primary_links) ?></div>
				<?php endif; ?>
		
		
		<div class="nav">
		<?php if (theme_get_setting('riebel_taskbar_ownlinks')): ?>
			<ul>
				<li><a href="">Link 1</a></li>
				<li><a href="">Link 2</a></li>
				<li><a href="">Link 3</a></li>
			</ul> 
		<?php endif; ?>
		<?php if (theme_get_setting('riebel_breadcrumb')): ?>
				<p>
					<?php if ($breadcrumb) { ?><div class="breadcrumbintaskbar"><?php print $breadcrumb ?></div><?php } ?>
				</p>
		<?php endif; ?>
      </div>
    </div>