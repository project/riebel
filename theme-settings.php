<?php
/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  /*
   * The default values for the theme variables. Make sure $defaults exactly
   * matches the $defaults in the template.php file.
   */
  $defaults = array(
    'riebel_slidetabs' => 1,
    'riebel_taskbar' => 0,
	'riebel_logo' => 0,
	'riebel_registerplease' => 0,
	'riebel_breadcrumb' => 0,
	'riebel_bg_image' => 'bg_bars_blue.png',
	'riebel_float' => 'none',
	'riebel_taskbar_links' => '',
  );

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // Create the form widgets using Forms API

  //--------------------------------------------------------------------------
  // Shows slidetabs in the header?
  $form['riebel_slidetabs'] = array(
    '#type' => 'checkbox',
    '#title' => t('display slidetabs'),
    '#default_value' => $settings['riebel_slidetabs'],
  );

  //--------------------------------------------------------------------------
  // Show a taskbar at the bootom of the screen?
  $form['riebel_taskbar'] = array(
    '#type' => 'checkbox',
    '#title' => t('display taskbar'),
    '#default_value' => $settings['riebel_taskbar'],
	'#description'   => t('The taskbar is an always visible bar at the bottom of the site. It can be configured by editing riebel_taskbar.tpl.php'),
  );

  //--------------------------------------------------------------------------
  // Show a graphical logo? If not the standard site name will be displayed.
  $form['riebel_registerplease'] = array(
    '#type' => 'checkbox',
    '#title' => t('display register please box to guests'),
    '#default_value' => $settings['riebel_registerplease'],
	'#description'   => t('If checked the site shows a register please box to guests.'),
  );
  
  //--------------------------------------------------------------------------
  // Show a graphical logo? If not the standard site name will be displayed.
  $form['riebel_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('display graphical logo'),
    '#default_value' => $settings['riebel_logo'],
	'#description'   => t('If checked the site shows a graphical logo which can be found here: img\riebel_logo.png.'),
  );

  //--------------------------------------------------------------------------
  // Show a graphical logo? If not the standard site name will be displayed.
  $form['riebel_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('display breadcrumb in taskbar'),
    '#default_value' => $settings['riebel_breadcrumb'],
	'#description'   => t('If checked the breadcrumb will be shown in the taskbar instead on top of the content. Note that you must have activated the taskbar, too!'), 
  );	

    //--------------------------------------------------------------------------
	// Background image
    $form['riebel_bg_image'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#default_value' => $settings['riebel_bg_image'],
    '#options' => array(
    'bg_bars_brown.png' => t('Brown'),
    'bg_bars_green.png' => t('Green'),
    'bg_bars_blue.png' => t('Blue'),
    'bg_bars_magenta.png' => t('Magenta'),
    'bg_bars_valentinelove.png' => t('Valentine Love'),
    'bg_bars_yellow.png' => t('Yellow'),
	'' => t('None'),
	),
	'#description'   => t('Set the background image. Your monitor resolution must be higher than 1024x768 to see them!'), 

  );
  
    //--------------------------------------------------------------------------
	// float
    $form['riebel_float'] = array(
    '#type' => 'select',
    '#title' => t('Float'),
    '#default_value' => $settings['riebel_float'],
    '#options' => array(
      'left' => t('Left'),
      'none' => t('None'),
      'right' => t('Right'),
	  ),
	'#description'   => t('Sets the position of the whole site.'),

  );
  
    //--------------------------------------------------------------------------
	// links in taskbar
    $form['riebel_taskbar_links'] = array(
    '#type' => 'select',
    '#title' => t('Taskbar Links'),
    '#default_value' => $settings['riebel_taskbar_links'],
    '#options' => array(
      'primary_links' => t('Primary Links'),
      'secondary_links' => t('Secondary Links'),
      '' => t('None of them'),
	  ),
	'#description'   => t('Defines which links should be displayed on the taskbar.'),
	);
	
  //--------------------------------------------------------------------------
  // own links in taskbar
  $form['riebel_taskbar_ownlinks'] = array(
    '#type' => 'checkbox',
    '#title' => t('display own links on taskbar'),
    '#default_value' => $settings['riebel_taskbar_ownlinks'],
	'#description'   => t('If you want to add own links to the taskbar you can check this here. You must edit riebel_taskbar.tpl.php otherwise the links wont work!'),
  );
  // Return the additional form widgets
  return $form;
}

?>