<?php // $Id:   ?>
<!-- start comment -->
<div class="comment-<?php print $row_class; print $comment->new ? ' comment-new forum-comment-new' : ''; ?>">
<div class="comment clearfix">
	<div class="comment-left">
		<?php if ($picture) { ?>
			<?php print $picture; ?>
		<?php } else { ?>
					<div style="text-align: center;" class="commentboxavatar"></div>
		<?php } ?>
				
	</div>
				
	<div class="comment-right">
		<div class="comment-meta">
			<span class="comment-author"><cite><?php print $author ?></cite></span>
			<?php /* Display comment title - deactivated is standard. uncomment it to activate it
			<span class="comment-title"><?php print $title; ?></span>
			*/
			?>
			<span class="comment-id"><?php print "#" . $id; ?></span>
			<br />
			<?php print $date ?>
		</div>

  					
  		<div class="comment-text">
			<div class="content"><?php print $content; ?></div>
				<?php if ($signature): ?>
					<div class="user-signature clear-block">
						<?php print $signature ?>
					</div>
				<?php endif; ?>
		</div>
		<div style="vertical-align:text-bottom;"><?php print $links; ?></div>
	</div>
</div><!-- end comment -->
</div>
